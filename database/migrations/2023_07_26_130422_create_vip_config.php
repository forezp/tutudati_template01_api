<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVipConfig extends Migration
{
    public function up()
    {
        Schema::create('user_vip_config', function (Blueprint $table) {
            $table->bigInteger("id", true, true);
            $table->bigInteger("uid", false, true)->unique("uid");
            $table->string("title", 255)->comment("会员等级");
            $table->decimal("money", 6)->default(0.00);
            $table->string("unit", 10)->comment("单位");
            $table->string("remark", 255)->comment("等级描述");
            $table->text("equity")->nullable()->comment("会员权益");
            $table->integer("sort", false, true)->default(0);
            $table->tinyInteger("is_show", false, true)->default(1);
            $table->dateTime("created_at");
            $table->dateTime("updated_at");
            $table->dateTime("deleted_at")->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('user_vip_config');
    }
}
