<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExamSubmitHistory extends Migration
{
    public function up()
    {
        Schema::create('ex_exam_submit_history', function (Blueprint $table) {
            $table->bigInteger("id", true, true);
            $table->bigInteger("uid", false, true)->unique("uid");
            $table->bigInteger("ex_exam_submit", false, true)->unique("ex_exam_submit")->comment("答题汇总uid");
            $table->bigInteger("user_uid", false, true)->index("user_uid")->comment("用户uid");
            $table->bigInteger("collection_uid", false, true)->index("collection_uid")->comment("试卷uid");
            $table->bigInteger("exam_uid", false, true)->index("exam_uid")->comment("试题uid");
            $table->string("type", 32)->index("type")->comment("试题类型");
            $table->string("answer", 255)->comment("答案");
            $table->decimal("score", 6)->default(0.00);
            $table->integer("sort", false, true)->default(0);
            $table->tinyInteger("is_show", false, true)->default(1);
            $table->dateTime("created_at");
            $table->dateTime("updated_at");
            $table->dateTime("deleted_at")->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ex_exam_submit');
    }
}
