<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <strong>备案号</strong>&nbsp;&nbsp; {!! config('app.app_beian') !!}&nbsp;&nbsp;&nbsp;
        @if(config('admin.show_environment'))
            <strong>软件环境</strong>&nbsp;&nbsp; {!! config('app.env') !!}&nbsp;&nbsp;&nbsp;
        @endif

        @if(config('admin.show_version'))
            <strong>软件版本</strong>&nbsp;&nbsp; {!! config("app.app_version") !!}
        @endif
    </div>
    <!-- Default to the left -->
    <strong>Powered by <a href="https://www.tutudati.com" target="_blank">兔兔答题</a></strong>
</footer>
