<!DOCTYPE html>
<html lang="zh-CN">

<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <meta name="renderer" content="webkit">
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <title>{{env("APP_SITE_NAME")}} - 程序员面试题宝典</title>
    <meta name="description"
          content="兔兔答题是一款，专做程序员面试答题，丰富的面试资料。包含Java，Go，PHP，Rust，Python，前端，运维，人工智能，大数据等相关面试真题的微信小程序。">
    <meta name="keywords"
          content="Java面试题，JavaScript面试题，PHP面试题，MySQL面试题，算法面试题，人工智能面试题，微信小程序答题，答题教育系统，答题小程序">

    <link rel="shortcut icon" href="{{env("APP_URL")}}image/favicon.ico">
    <link rel="apple-touch-icon-precomposed" href="{{env("APP_URL")}}image/logo.png"/>
    <link rel="apple-touch-icon" sizes="76x76" href="{{env("APP_URL")}}image/logo.png"/>
    <link rel="apple-touch-icon" sizes="120x120" href="{{env("APP_URL")}}image/logo.png"/>
    <link rel="apple-touch-icon" sizes="152x152" href="{{env("APP_URL")}}image/logo.png"/>
    <link rel="apple-touch-icon" sizes="180x180" href="{{env("APP_URL")}}image/logo.png"/>
    <meta content='{{env("APP_SITE_NAME")}}' name='Author'/>

    <link href="{{env("APP_URL")}}web/css/style.css" rel="stylesheet" type="text/css"/>

    <meta name='robots' content='max-image-preview:large'/>

    <style type="text/css">
        img.wp-smiley, img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 0.07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>

<body class="home blog">
{{--    <div id="app">--}}
{{--        <example-component></example-component>--}}
{{--    </div>--}}
<div class="box-wrap">
    {{--    左侧菜单导航开始--}}
    <div class="header">
        <div class="logo-wrap">
            <a href="{{env("APP_URL")}}" title="专做程序员面试题" class="logo-img-wrap">
                <img src="{{env("APP_URL")}}image/logo.png" alt="兔兔答题" class="logo">
                <h1>兔兔答题</h1>
            </a>
            <div class="sub-title">专做程序员面试题</div>
        </div>

        <div class="menu-header-container">
            <ul id="menu-header" class="menu">
                <li id="menu-item-23"
                    class="menu-item menu-item-type-taxonomy menu-item-object-category current-menu-item menu-item-23">
                    <a title="程序面试题" href="{{env("APP_URL")}}">首页</a>
                </li>
                <li id="menu-item-25" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-25">
                    <a title="程序技术文章" href="{{env("APP_URL")}}collection/list">答题</a>
                </li>
                <li id="menu-item-3667"
                    class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-3667">
                    <a title="程序员交流" href="{{env("APP_URL")}}article/list">资讯</a>
                </li>
                <li id="menu-item-24" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-24">
                    <a title="程序员推荐" href="{{env("APP_URL")}}article/list">推荐</a>
                </li>
            </ul>
        </div>

        <div class="wx-code">
            <i class="tficon icon-qrcode"></i>
            扫码答题
            <img src="{{env("WEB_MENU_LOGO")}}" alt="兔兔答题微信小程序" class="wx-code-img">
        </div>
    </div>
    {{--    左侧菜单导航结束--}}

    {{--    主题内容开始--}}
    <div class="main">
        @if(count($bannerList))
            <div class="pic-cover-list slider-container">
                <div class="slider">
                    @foreach($bannerList as $value)
                        <a href="{{$value["navigate"]}}" class="pic-cover-item slider__item" target="_blank">
                            <img
                                src="{{$value["url"]}}{{$value["path"]}}"
                                alt="{{$value["first_title"]}}{{$value["second_title"]}}"
                                class="pic-cover-item-img"/>
                            <div class="slider__caption">
                                <h3 class="pic-cover-item-title">{{$value["first_title"]}}{{$value["second_title"]}}</h3>
                            </div>
                        </a>
                    @endforeach
                </div>
                <div class="slider__switch slider__switch--prev" data-ikslider-dir="prev">
                <span>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                      <path
                          d="M13.89 17.418c.27.272.27.71 0 .98s-.7.27-.968 0l-7.83-7.91c-.268-.27-.268-.706 0-.978l7.83-7.908c.268-.27.7-.27.97 0s.267.71 0 .98L6.75 10l7.14 7.418z"/>
                   </svg>
                </span>
                </div>
                <div class="slider__switch slider__switch--next" data-ikslider-dir="next">
                <span>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                      <path
                          d="M13.25 10L6.11 2.58c-.27-.27-.27-.707 0-.98.267-.27.7-.27.968 0l7.83 7.91c.268.27.268.708 0 .978l-7.83 7.908c-.268.27-.7.27-.97 0s-.267-.707 0-.98L13.25 10z"/>
                   </svg>
                </span>
                </div>
            </div>
        @endif

        <!-- 横条广告 -->
        {{--        <ins class="adsbygoogle"--}}
        {{--             style="display:inline-block;min-width:400px;max-width:720px;width:100%;height:260px"--}}
        {{--             data-ad-client="ca-pub-4401169466922752"--}}
        {{--             data-ad-slot="8614034451"--}}
        {{--             data-ad-format="auto"--}}
        {{--             data-full-width-responsive="true"></ins>--}}
        {{--        <script>--}}
        {{--            (adsbygoogle = window.adsbygoogle || []).push({});--}}
        {{--        </script>--}}
        {{--        首页试题开始--}}
        <ul id="menu-media" class="cat-tab-wrap">
            <li id="menu-item-3664"
                class="menu-item menu-item-type-taxonomy menu-item-object-category current-menu-item menu-item-3664"><a
                    title="优质试题推荐" href="{{env("APP_URL")}}" aria-current="page">优质试题推荐</a></li>
        </ul>
        <div class="post-list">
            @foreach($collectionList as $value)
                <div class="post-item">
                    <div class="post-item-cover">
                        <a class="post-item-img" href="{{env("APP_URL")}}collection/detail/{{$value["uid"]}}"
                           title="{{$value["title"]}}"
                           target="_blank">
                            <img class="hover-scale" src="{{$value["url"]}}{{$value["path"]}}"
                                 alt="{{$value["title"]}}" height="100%"></a>
                        <ul class="post-categories">
                            <li><a href="{{env("APP_URL")}}collection/detail/{{$value["uid"]}}"
                                   rel="category tag">{{$value["category"]["title"]}}
                                    面试试题</a></li>
                        </ul>
                    </div>
                    <a href="{{env("APP_URL")}}collection/detail/{{$value["uid"]}}" class="post-item-title"
                       title="{{$value["title"]}}"
                       target="_blank">
                        <h3>{{$value["title"]}}</h3>
                    </a>
                    <div class="post-item-footer">
                        <div class="tag-wrap">
                            <a href="{{env("APP_URL")}}collection/detail/{{$value["uid"]}}"
                               rel="tag">{{$value["category"]["title"]}}</a>
                            <a href="{{env("APP_URL")}}collection/detail/{{$value["uid"]}}"
                               rel="tag">{{$value["author"]}}</a>
                            <a href="{{env("APP_URL")}}collection/detail/{{$value["uid"]}}" rel="tag">
                                @if($value["level"] == 1)
                                    简单
                                @elseif($value["level"] == 2)
                                    中等
                                @elseif($value["level"] == 3)
                                    较难
                                @endif
                            </a>
                        </div>
                        <div
                            class="post-item-meta">@php echo date("Y-m-d", strtotime($value["created_at"])); @endphp</div>
                    </div>
                    <p class="post-item-summary">{{$value["remark"]}}</p>
                </div>
            @endforeach
        </div>
        {{--        首页试题结束--}}
        <ul id="menu-media" class="cat-tab-wrap">
            <li id="menu-item-3664"
                class="menu-item menu-item-type-taxonomy menu-item-object-category current-menu-item menu-item-3664"><a
                    title="优质文章推荐" href="{{env("APP_URL")}}" aria-current="page">优质文章推荐</a></li>
        </ul>
        {{--        首页文章开始--}}
        <div class="post-list">
            @foreach($articleList as $value)
                <div class="post-item">
                    <div class="post-item-cover">
                        <a class="post-item-img" href="{{env("APP_URL")}}article/detail/{{$value["uid"]}}"
                           title="{{$value["title"]}}"
                           target="_blank">
                            <img class="hover-scale" src="{{$value["url"]}}{{$value["path"]}}"
                                 alt="{{$value["title"]}}"></a>
                        <ul class="post-categories">
                            <li><a href="{{env("APP_URL")}}article/detail/{{$value["uid"]}}"
                                   rel="category tag">{{$value["category"]["title"]}}
                                    资讯文章</a></li>
                        </ul>
                    </div>
                    <a href="{{env("APP_URL")}}article/detail/{{$value["uid"]}}" class="post-item-title"
                       title="{{$value["title"]}}"
                       target="_blank">
                        <h3>{{$value["title"]}}</h3>
                    </a>
                    <div class="post-item-footer">
                        <div class="tag-wrap">
                            <a href="{{env("APP_URL")}}article/detail/{{$value["uid"]}}"
                               rel="tag">{{$value["category"]["title"]}}</a>
                            <a href="{{env("APP_URL")}}article/detail/{{$value["uid"]}}"
                               rel="tag">{{$value["author"]}}</a>
                            @foreach($value["keywords"] as $v)
                                <a href="{{env("APP_URL")}}article/detail/{{$value["uid"]}}" rel="tag">{{$v}}</a>
                            @endforeach
                        </div>
                        <div
                            class="post-item-meta">@php echo date("Y-m-d", strtotime($value["created_at"])); @endphp</div>
                    </div>
                    <p class="post-item-summary">{{$value["remark"]}}</p>
                </div>
            @endforeach
        </div>
        {{--        首页文章结束--}}
        {{--        首页分页开始--}}
        {{--        <ul class="pagination">--}}
        {{--            <li class="prev-page"></li>--}}
        {{--            <li class="active"><span>1</span></li>--}}
        {{--            <li><a href='page/2.html' title=第2页>2</a></li>--}}
        {{--            <li><a href='page/3.html' title=第3页>3</a></li>--}}
        {{--            <li><a href='page/4.html' title=第4页>4</a></li>--}}
        {{--            <li><a href='page/5.html' title=第5页>5</a></li>--}}
        {{--            <li><span class="more">···</span></li>--}}
        {{--            <li><a href='page/32.html' title=尾页>32</a></li>--}}
        {{--            <li class="next-page"><a href="page/2.html"><i class="tficon icon-right"></i></a></li>--}}
        {{--            <div class="total-page">共32页</div>--}}
        {{--        </ul>--}}
        {{--        首页分页结束--}}
    </div>
    {{--    主题内容结束--}}

    {{--    侧边栏开始--}}
    <div class="aside">
        {{--        侧边栏顶部开始--}}
        <div class="aside-block">

            {{--            侧边栏搜索框开始--}}
            <div class="top-bar">
                <form class="search-form" method="get" action="/">
                    <button class="tficon icon-search" type="submit"></button>
                    <input type="text" name="s" class="search-input" placeholder="输入关键词，回车搜索" value="">
                </form>
            </div>
            {{--            侧边栏搜索狂框结束--}}

            {{--            侧边栏三小图开始--}}
            <div class="block-wrap">
                <h2 class="block-title">
                    <a href="{{env("APP_URL")}}/collection/list">推荐试题<i class="tficon icon-right"></i></a>
                </h2>
                <div class="photo-list">
                    @foreach($recommendList as $value)
                        <a href="{{env("APP_URL")}}collection/detail/{{$value["uid"]}}" class="photo-item"
                           target="_blank">
                            <img
                                src="{{$value["url"]}}{{$value["path"]}}"
                                alt="{{$value["title"]}}" class="photo-item-img hover-scale">
                            <div class="photo-item-inner">
                                <h3 class="photo-item-title">{{$value["category"]["title"]}}</h3>
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>
            {{--            侧边栏三小图结束--}}

            {{--            侧边栏三小图1开始--}}
            <div class="block-wrap">
                <h2 class="block-title">
                    <a href="{{env("APP_URL")}}/collection/list">热门试题<i class="tficon icon-right"></i></a>
                </h2>
                <div class="fonts-list">
                    @foreach($hotList as $value)
                        <a href="{{env("APP_URL")}}collection/detail/{{$value["uid"]}}" class="fonts-item"
                           title="{{$value["title"]}}">
                            <img src="{{$value["url"]}}{{$value["path"]}}" alt="{{$value["title"]}}"
                                 class="fonts-item-img hover-scale" width="143" height="143">
                        </a>
                    @endforeach
                </div>
            </div>
            {{--            侧边栏三小图1结束--}}
        </div>
        {{--        侧边栏顶部结束--}}

        <div class="wwads-cn wwads-horizontal" data-id="195" style="max-width:416px"></div>

        {{--        侧边栏推荐开始--}}
        <div class="aside-block block-wrap">
            <h2 class="block-title">
                <a href="{{env("APP_URL")}}article/list">热门文章<i class="tficon icon-right"></i></a>
            </h2>

            <div class="sidebar-post-list">
                @foreach($hotArticleList as $value)
                    <div class="sider-post-item">
                        <a class="sider-post-item-img" href="{{env("APP_URL")}}article/detail/{{$value["uid"]}}"
                           title="{{$value["title"]}}">
                            <img class="hover-scale"
                                 src="{{$value["url"]}}{{$value["path"]}}"
                                 alt="{{$value["title"]}}">
                        </a>
                        <a class="sider-post-item-title" href="{{env("APP_URL")}}article/detail/{{$value["uid"]}}"
                           title="{{$value["title"]}}">
                            <h3>{{$value["title"]}}</h3>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
        {{--        侧边栏推荐结束--}}

        {{--        侧边栏广告开始--}}
        {{--        <div class="aside-block aside-ddd block-wrap">--}}

        {{--            <!-- 侧栏竖条广告 -->--}}
        {{--            <ins class="adsbygoogle aside-ddd-clum"--}}
        {{--                 style="display:block"--}}
        {{--                 data-ad-client="ca-pub-4401169466922752"--}}
        {{--                 data-ad-slot="4195151876"--}}
        {{--                 data-ad-format="auto"--}}
        {{--                 data-full-width-responsive="true"></ins>--}}
        {{--            <script>--}}
        {{--                (adsbygoogle = window.adsbygoogle || []).push({});--}}
        {{--            </script>--}}
        {{--        </div>--}}
        {{--        侧边栏广告结束--}}

    </div>
    {{--    侧边栏结束--}}
    <link href="{{env("APP_URL")}}web/css/slider.css" rel="stylesheet" type="text/css"/>
    <script src="{{env("APP_URL")}}web/js/jquery.min.js"></script>
    @if(count($bannerList))
        <script src="{{env("APP_URL")}}web/js/slider.js"></script>
        <script type="text/javascript">
            $(".slider-container").ikSlider({
                speed: 500,
                delay: 3000,
                infinite: true
            });
        </script>
    @endif
</div>
<div class="footer">
    Copy Right©2023, All Rights Reserved.  {{request()->getHost()}}
    <br class="footer-br"/>
    <a href="https://beian.miit.gov.cn/" target="_blank" ref="nofollow">蜀ICP备16032791号</a>
    @foreach(app("App\Logic\Web\Config\FriendlyService")->getList() as $value)
        <a href="{{$value['site_url']}}" target="_blank" ref="nofollow">{{$value["title"]}}</a>
    @endforeach
</div>

</body>
<script>
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?1ce8a30e5d1be9b36c3c252cce920a49";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

</html>
