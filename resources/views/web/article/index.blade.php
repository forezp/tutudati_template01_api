<!DOCTYPE html>
<html lang="zh-CN">

<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <meta name="renderer" content="webkit">
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <title>{{env("APP_SITE_NAME")}} - {{$seoInfo["seo_title"]}}</title>
    <meta name="description" content="{{$seoInfo["seo_description"]}}">
    <meta name="keywords" content="{{$seoInfo["seo_keywords"]}}">
    <link rel="shortcut icon" href="{{env("APP_URL")}}image/favicon.ico">
    <link rel="apple-touch-icon-precomposed" href="{{env("APP_URL")}}image/logo.png"/>
    <link rel="apple-touch-icon" sizes="76x76" href="{{env("APP_URL")}}image/logo.png"/>
    <link rel="apple-touch-icon" sizes="120x120" href="{{env("APP_URL")}}image/logo.png"/>
    <link rel="apple-touch-icon" sizes="152x152" href="{{env("APP_URL")}}image/logo.png"/>
    <link rel="apple-touch-icon" sizes="180x180" href="{{env("APP_URL")}}image/logo.png"/>
    <meta content='{{env("APP_SITE_NAME")}}' name='Author'/>

    <link href="{{env("APP_URL")}}web/css/style.css" rel="stylesheet" type="text/css"/>

    <meta name='robots' content='max-image-preview:large'/>

    <style type="text/css">
        img.wp-smiley, img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 0.07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>

<body class="home blog">
<div class="box-wrap">
    {{--    左侧菜单导航开始--}}
    <div class="header">
        <div class="logo-wrap">
            <a href="{{env("APP_URL")}}" title="专做程序员面试题" class="logo-img-wrap">
                <img src="{{env("APP_URL")}}image/logo.png" alt="兔兔答题" class="logo">
                <h1>兔兔答题</h1>
            </a>
            <div class="sub-title">专做程序员面试题</div>
        </div>

        <div class="menu-header-container">
            <ul id="menu-header" class="menu">
                <li id="menu-item-23"
                    class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-23">
                    <a title="程序面试题" href="{{env("APP_URL")}}">首页</a>
                </li>
                <li id="menu-item-25" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-25">
                    <a title="程序技术文章" href="{{env("APP_URL")}}collection/list">答题</a>
                </li>
                <li id="menu-item-3667"
                    class="menu-item menu-item-type-taxonomy menu-item-object-category current-menu-item menu-item-3667">
                    <a title="程序员交流" href="{{env("APP_URL")}}article/list">资讯</a>
                </li>
                <li id="menu-item-24" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-24">
                    <a title="程序员推荐" href="{{env("APP_URL")}}article/list">推荐</a>
                </li>
            </ul>
        </div>

        <div class="wx-code">
            <i class="tficon icon-qrcode"></i>
            扫码答题
            <img src="{{env("WEB_MENU_LOGO")}}" alt="兔兔答题微信小程序"
                 class="wx-code-img">
        </div>
    </div>
    {{--    左侧菜单导航结束--}}

    {{--    主体内容开始--}}
    <div class="main">
        <div class="top-bar">
            <div class="crumbs">
                <a href="{{env("APP_URL")}}">首页</a>
                <h2>资讯列表</h2>
            </div>
            <form class="search-form" method="get" action="{{request()->url()}}">
                <button class="tficon icon-search" type="submit"></button>
                <input type="text" name="s" class="search-input" placeholder="输入关键词，回车搜索" value="">
            </form>
        </div>

        <ul id="menu-media" class="cat-tab-wrap">
            @foreach($categoryList as $value)
                <li id="menu-item-3664"
                    class="menu-item menu-item-type-taxonomy menu-item-object-category @if(($cate_uid == "" && $value["uid"] == "") || ($value["uid"] == $cate_uid)) current-menu-item @endif  menu-item-3664">
                    <a title="{{$value["title"]}}" href="{{env("APP_URL")}}article/list/{{$value["uid"]}}"
                       aria-current="page">{{$value["title"]}}
                    </a>
                </li>
            @endforeach
        </ul>
        <!-- 横条广告 -->
        {{--        <ins class="adsbygoogle"--}}
        {{--             style="display:inline-block;min-width:400px;max-width:720px;width:100%;height:260px"--}}
        {{--             data-ad-client="ca-pub-4401169466922752"--}}
        {{--             data-ad-slot="8614034451"--}}
        {{--             data-ad-format="auto"--}}
        {{--             data-full-width-responsive="true"></ins>--}}
        {{--        <script>--}}
        {{--            (adsbygoogle = window.adsbygoogle || []).push({});--}}
        {{--        </script>--}}
        @if(count($articleList))
            <div class="post-list">
                @foreach($articleList as $value)
                    <div class="post-item">
                        <div class="post-item-cover">
                            <a class="post-item-img" href="{{env("APP_URL")}}article/detail/{{$value["uid"]}}"
                               title="{{$value["title"]}}" target="_blank">
                                <img class="hover-scale" src="{{$value["url"]}}{{$value["path"]}}"
                                     alt="{{$value["title"]}}"></a>
                            <ul class="post-categories">
                                <li>
                                    <a href="{{env("APP_URL")}}article/detail/{{$value["uid"]}}"
                                       rel="category tag">{{$value["category"]["title"]}}
                                        资讯文章</a>
                                </li>
                            </ul>
                        </div>
                        <a href="{{env("APP_URL")}}article/detail/{{$value["uid"]}}" class="post-item-title"
                           title="{{$value["title"]}}"
                           target="_blank">
                            <h3>{{$value["title"]}}</h3>
                        </a>
                        <div class="post-item-footer">
                            <div class="tag-wrap">
                                <a href="{{env("APP_URL")}}article/detail/{{$value["uid"]}}"
                                   rel="tag">{{$value["category"]["title"]}}</a>
                                <a href="{{env("APP_URL")}}article/detail/{{$value["uid"]}}"
                                   rel="tag">{{$value["author"]}}</a>
                                @foreach($value["keywords"] as $v)
                                    <a href="{{env("APP_URL")}}article/detail/{{$value["uid"]}}" rel="tag">{{$v}}</a>
                                @endforeach
                            </div>
                            <div
                                class="post-item-meta">@php echo date("Y-m-d", strtotime($value["created_at"])); @endphp</div>
                        </div>
                        <p class="post-item-summary">{{$value["remark"]}}</p>
                    </div>
                @endforeach
            </div>
        @else
            <div class="list-empty">
                内容不存在
            </div>
        @endif
        @if(count($articleList))
            <ul class="pagination">
                {{ $articleList->appends(["s" => request("s")])->links() }}
                <li class="prev-page"></li>
                {{--            <li class="active"><span>1</span></li>--}}
                {{--            <li><a href='material/page/2.html' title=第2页>2</a></li>--}}
                {{--            <li><a href='material/page/3.html' title=第3页>3</a></li>--}}
                {{--            <li class="next-page"><a href="material/page/2.html"><i class="tficon icon-right"></i></a></li>--}}
                <div class="total-page">共{{$articleList->total()}}条</div>
            </ul>
        @endif
    </div>
    {{--主体内容结束--}}

    {{--    侧边栏开始--}}
    <div class="aside">

        <div class="aside-block">
            <h2 class="block-title">
                <i class="tficon icon-fire-line"></i> 热门文章
            </h2>

            <div class="sidebar-post-list">
                @foreach($hotList as $value)
                    <div class="sider-post-item">
                        <a class="sider-post-item-img" href="{{env("APP_URL")}}article/detail/{{$value["uid"]}}"
                           title="{{$value["title"]}}">
                            <img class="hover-scale"
                                 src="{{$value["url"]}}{{$value["path"]}}"
                                 alt="{{$value["title"]}}">
                        </a>
                        <a class="sider-post-item-title" href="{{env("APP_URL")}}article/detail/{{$value["uid"]}}"
                           title="{{$value["title"]}}">
                            <h3>{{$value["title"]}}</h3>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="wwads-cn wwads-horizontal" data-id="195" style="max-width:416px"></div>

        {{--        <div class="aside-block aside-ddd block-wrap">--}}

        {{--            <!-- 侧栏竖条广告 -->--}}
        {{--            <ins class="adsbygoogle aside-ddd-clum"--}}
        {{--                 style="display:block"--}}
        {{--                 data-ad-client="ca-pub-4401169466922752"--}}
        {{--                 data-ad-slot="4195151876"--}}
        {{--                 data-ad-format="auto"--}}
        {{--                 data-full-width-responsive="true"></ins>--}}
        {{--            <script>--}}
        {{--                (adsbygoogle = window.adsbygoogle || []).push({});--}}
        {{--            </script>--}}
        {{--        </div>--}}

    </div>
    {{--    侧边栏结束--}}
</div>
<div class="footer">
    Copy Right©2023, All Rights Reserved. {{request()->getHost()}}
    <br class="footer-br"/>
    <a href="https://beian.miit.gov.cn/" target="_blank" ref="nofollow">蜀ICP备16032791号</a>
    @foreach(app("App\Logic\Web\Config\FriendlyService")->getList() as $value)
        <a href="{{$value['site_url']}}" target="_blank" ref="nofollow">{{$value["title"]}}</a>
    @endforeach
</div>

</body>
<script>
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?1ce8a30e5d1be9b36c3c252cce920a49";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

</html>
