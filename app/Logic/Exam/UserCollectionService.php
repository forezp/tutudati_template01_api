<?php
declare(strict_types=1);

namespace App\Logic\Exam;

use App\Library\SnowFlakeId;
use App\Logic\BaseUserService;
use App\Model\User\Exam\UserCollection;
use App\Model\User\Exam\UserCollectionCollectionHistory;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/22
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class UserCollectionService extends BaseUserService
{
    public function submitCollection(): int
    {
        $requestParams = request()->all();
        $userCollection = UserCollectionCollectionHistory::query()->where([
            ["user_uid", "=", $this->getUserUid()],
            ["collection_uid", "=", $requestParams["uid"]]
        ])->first(["id"]);
        if (!empty($userCollection)) {
            return 1;//已收藏
        }
        $model = UserCollectionCollectionHistory::query()->create([
            "user_uid" => $this->getUserUid(),
            "collection_uid" => $requestParams["uid"],
            "uid" => SnowFlakeId::getId(),
        ]);
        if (!empty($model->getKey())) {
            UserCollection::query()->where("uid", "=", $requestParams["uid"])->increment("collect");
            return 2; // 收藏成功
        }
        return 3;// 收藏失败
    }
}
