<?php

declare(strict_types=1);

namespace App\Logic\Exam;

use App\Logic\BaseUserService;
use App\Model\User\Exam\UserCollectionJudeRel;
use App\Model\User\Exam\UserJude;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/24
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class JudeService extends BaseUserService
{
    public function getList(): array
    {
        $requestParams = request()->all();
        $examUid = UserCollectionJudeRel::query()
            ->where("collection_uid", "=", $requestParams["collection_uid"])
            ->get(["exam_uid"])
            ->toArray();
        if (empty($examUid)) {
            return [];
        }
        $examUidArray = array_column($examUid, "exam_uid");
        return UserJude::query()
            ->whereIn("uid", $examUidArray)
            ->where([
                ["is_show", "=", 1],
            ])
            ->orderByDesc("sort")
            ->orderByDesc("id")
            ->get(["uid", "option", "score", "analysis", "is_correct", "title"])
            ->toArray();
    }
}
