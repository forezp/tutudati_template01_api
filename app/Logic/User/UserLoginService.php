<?php
declare(strict_types=1);

namespace App\Logic\User;

use App\Library\MiniWeChatClient;
use App\Library\SnowFlakeId;
use App\Model\User\Config\UserProfession;
use App\Model\User\User\UserModel;
use Illuminate\Support\Facades\Cache;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/20
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class UserLoginService
{
    public function login(): array
    {
        $requestParams = request()->all();
        if (empty($requestParams["code"])) {
            return ["errcode" => 1, "errmsg" => "登录code缺失"];
        }
        $loginInfo = MiniWeChatClient::client()->auth->session($requestParams["code"]);
        if (!empty($loginInfo["openid"])) {
            $userInfo = UserModel::query()->where([["openid", "=", $loginInfo["openid"]]])->first();
            $uid = $userInfo["uid"] ?? SnowFlakeId::getId();
            $nickname = $userInfo["nickname"] ?? "用户" . mt_rand(100, 20000);
            $remark = empty($userInfo["remark"]) ? "这家伙很懒 什么都没留下" : $userInfo["remark"];
            $profession = $userInfo["profession"] ?? 0;
            $code = $userInfo["code"] ?? substr(SnowFlakeId::getId(), -10);
            if (empty($userInfo)) {
                $professionConfig = UserProfession::query()->first(["uid", "title"])->toArray();
                $userModel = UserModel::query()->create([
                    "uid" => $uid,
                    "openid" => $loginInfo["openid"],
                    "nickname" => $nickname,
                    "remark" => $remark,
                    "code" => $code,
                    "profession_uid" => !empty($professionConfig) ? $professionConfig["uid"] : 0,
                ]);
                if (empty($userModel->getKey())) {
                    return ["errcode" => 1, "errmsg" => "创建用户信息失败"];
                }
                $profession = [
                    "title" => $professionConfig["title"],
                    "uid" => $professionConfig["uid"]
                ];
            }
            $userLoginCacheInfo = [
                "uid" => $uid,
                "openid" => $loginInfo["openid"],
                "nickname" => $nickname,
                "mobile" => empty($userInfo["mobile"]) ? "" : $userInfo["mobile"],
                "email" => empty($userInfo["email"]) ? "" : $userInfo["email"],
                "avatar_url" => $userInfo["avatar_url"] ?? "https://img1.imgtp.com/2023/08/23/WGGzh8E7.png",
                "remark" => $remark,
                "profession" => $profession,
                "code" => $code,
            ];
            $tokenKey = SnowFlakeId::getId();
            $setLoginCache = Cache::put("login:" . $tokenKey, $userLoginCacheInfo, 7 * 86400);
            if ($setLoginCache) {
                return [
                    "errcode" => 0,
                    "errmsg" => "登录成功",
                    "data" => [
                        "token" => $tokenKey,
                        "userinfo" => [
                            "nickname" => $nickname,
                            "avatar_url" => $userInfo["avatar_url"] ?? "https://img1.imgtp.com/2023/08/23/WGGzh8E7.png",
                        ],
                    ]];
            }
        }
        return ["errcode" => 1, "errmsg" => "登录失败"];
    }
}
