<?php
declare(strict_types=1);

namespace App\Logic;

use App\Model\User\User\UserModel;
use Illuminate\Support\Facades\Session;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/19
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class BaseUserService
{
    protected function userInfo(): array
    {
        if (!empty(Session::get("userinfo"))) {
            return Session::get("userinfo");
        }
        return [];
    }

    protected function getUserUid(): string
    {
        if (!empty(Session::get("userinfo"))) {
            return (string)Session::get("userinfo")["uid"];
        }
        return "";
    }

    protected function getVipState(): int
    {
        $usrVipInfo = UserModel::query()
            ->where("uid", "=", $this->getUserUid())
            ->first(["is_vip", "vip_starttime", "vip_endtime"]);
        if (!empty($usrVipInfo["vip_endtime"])) {
            if ($usrVipInfo["is_vip"] == 1 && time() < strtotime($usrVipInfo["vip_endtime"])) {
                return 1;
            }
        }
        return 2;
    }
}
