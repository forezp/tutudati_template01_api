<?php
declare(strict_types=1);

namespace App\Logic\Web\Config;

use App\Logic\BaseUserService;
use App\Model\User\Config\UserBanner;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/19
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class BannerService extends BaseUserService
{
    public function getList(array $requestParams): array
    {
        return UserBanner::query()
            ->where([
                ["is_show", "=", 1],
                ["position", "=", $requestParams["position"]]
            ])
            ->orderByDesc("sort")
            ->get(["url", "path", "first_title", "second_title", "navigate"])
            ->toArray();
    }
}
