<?php
declare(strict_types=1);

namespace App\Logic\Config;

use App\Logic\BaseUserService;
use App\Model\User\Config\UserProfession;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/22
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class UserProfessionService extends BaseUserService
{
    public function getList(): array
    {
        return UserProfession::query()
            ->where("is_show", "=",1)
            ->get(["uid", "title"])
            ->toArray();
    }
}
