<?php
declare(strict_types=1);

namespace App\Logic\Config;

use App\Logic\BaseUserService;
use App\Model\User\Config\UserMenu;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/19
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class MenuService extends BaseUserService
{
    public function getList(): array
    {
        return UserMenu::query()
            ->where([
                ["is_show", "=", 1]
            ])
            ->orderByDesc("sort")
            ->get(["url", "path", "title", "navigate"])
            ->toArray();
    }
}
