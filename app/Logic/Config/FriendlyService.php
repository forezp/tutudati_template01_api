<?php
declare(strict_types=1);

namespace App\Logic\Config;

use App\Logic\BaseUserService;
use App\Model\User\Config\UserFriendly;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/19
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class FriendlyService extends BaseUserService
{
    public function getList(): array
    {
       return UserFriendly::query()->where([
            ["is_show", "=", 1]
        ])->orderByDesc("sort")
            ->get(["title", "url", "path", "site_url"])
            ->toArray();
    }
}
