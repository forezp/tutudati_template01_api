<?php

declare(strict_types=1);

namespace App\Logic\Message;

use App\Library\SnowFlakeId;
use App\Logic\BaseUserService;
use App\Model\User\Message\UserTemplate;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/5/25
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class TemplateService extends BaseUserService
{
    public function submitSubscribe(): bool
    {
        $requestParams = request()->all();
        if (empty($requestParams["template_id"])) {
            return false;
        }
        $model =  UserTemplate::query()->create([
            "user_uid" => $this->getUserUid(),
            "openid" => $this->userInfo()["openid"],
            "template_id" => $requestParams["template_id"],
            "uid" => SnowFlakeId::getId(),
        ]);
        return !empty($model->getKey());
    }
}
