<?php
declare(strict_types=1);

namespace App\Http\Controllers\Config;

use App\Http\Controllers\BaseController;
use App\Logic\Config\MenuService;
use Illuminate\Http\JsonResponse;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/19
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class MenuController extends BaseController
{
    public function getList(): JsonResponse
    {
        return $this->success((new MenuService())->getList());
    }
}
