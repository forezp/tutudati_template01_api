<?php
declare(strict_types = 1);
namespace App\Http\Controllers\Exam;

use App\Http\Controllers\BaseController;
use App\Http\Requests\Exam\CollectionCategoryValidate;
use App\Http\Requests\Exam\CollectionUidValidate;
use App\Logic\Exam\CollectionService;
use Illuminate\Http\JsonResponse;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/20
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class CollectionController extends BaseController
{
    public function getList(CollectionCategoryValidate  $validate): JsonResponse
    {
        return $this->success((new CollectionService)->getList());
    }

    public function getRelList(): JsonResponse
    {
        return $this->success((new CollectionService)->getRelList());
    }

    public function content(CollectionUidValidate $validate): JsonResponse
    {
        return $this->success((new CollectionService())->content());
    }

    public function submitGroupList(CollectionUidValidate  $validate): JsonResponse
    {
        return $this->success((new CollectionService())->submitGroupList());
    }

    // 获取试卷套题
    public function getCollectionExamList(): JsonResponse
    {
        return $this->success((new CollectionService())->getCollectionExamList());
    }

    public function getCollectionList()
    {
        // 查询条件
        // 1. 排序字段，是否免费，难度，答题人数
        // 2. 查询字段，试题分类，试题年份
    }
}
