<?php
declare(strict_types=1);

namespace App\Http\Controllers\Exam;

use Illuminate\Http\JsonResponse;
use App\Logic\Exam\ReadingService;
use App\Http\Controllers\BaseController;
use App\Http\Requests\Exam\CollectionCommonValidate;
use App\Http\Requests\Exam\ReadingContentValidate;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/20
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class ReadingController extends BaseController
{
    public function getList(CollectionCommonValidate $validate): JsonResponse
    {
        return $this->success((new ReadingService())->getList());
    }

    public function content(ReadingContentValidate $validate): JsonResponse
    {
        return $this->success((new ReadingService())->content());
    }

    public function submitGroupList(ReadingContentValidate $validate): JsonResponse
    {
        return $this->success((new ReadingService)->submitGroupList());
    }
}
