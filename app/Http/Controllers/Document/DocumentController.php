<?php
declare(strict_types=1);

namespace App\Http\Controllers\Document;

use App\Http\Controllers\BaseController;
use App\Logic\Document\DocumentService;
use Illuminate\Http\JsonResponse;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/19
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class DocumentController extends BaseController
{
    public function getList(): JsonResponse
    {
        return $this->success();
    }

    public function getContent(): JsonResponse
    {
        return $this->success((new DocumentService())->getContent());
    }
}
