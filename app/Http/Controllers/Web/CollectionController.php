<?php
declare(strict_types=1);

namespace App\Http\Controllers\Web;

use App\Logic\Web\Exam\CategoryService;
use App\Logic\Web\Exam\CollectionService;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/27
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class CollectionController
{
    public function index($id = 0)
    {
        $categoryList = [["uid" => "", "title" => "全部"]];
        array_push($categoryList, ...(new CategoryService())->getList());
        $requestParams = request()->all();
        $title = $requestParams["s"] ?? "";
        $collectionService = new CollectionService();
        $collectionList = $collectionService->getList(["title" => $title, "category_uid" => $id]);
        $hotCollectionList = $collectionService->getHostList(["category_uid" => $id]);
        $seoInfo = (new CategoryService())->getSeoInfo((string)$id);
        return view("web.collection.index", [
            "cateList" => $categoryList,
            "cate_uid" => $id,
            "collectionList" => $collectionList,
            "hotCollectionList" => $hotCollectionList,
            "seoInfo" => $seoInfo,
        ]);
    }

    public function detail($id)
    {
        $collectionService = new CollectionService();
        return view("web.collection.detail", [
            "content" => $collectionService->getContent(["uid" => $id]),
            "relList" => $collectionService->getContentRecommendList(["uid" => $id]),
        ]);
    }
}
