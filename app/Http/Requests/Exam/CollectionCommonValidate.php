<?php
declare(strict_types=1);
namespace App\Http\Requests\Exam;

use App\Http\Requests\BaseValidate;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/21
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class CollectionCommonValidate extends BaseValidate
{

    public function rules(): array
    {
        return [
            "collection_uid" => "required",
        ];
    }

    public function messages(): array
    {
        return [
            "collection_uid.required" => "试卷编号不能为空",
        ];
    }
}
