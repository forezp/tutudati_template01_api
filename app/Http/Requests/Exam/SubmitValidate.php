<?php

namespace App\Http\Requests\Exam;

use App\Http\Requests\BaseValidate;

/**
 * 考试答题记录
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/08/04
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class SubmitValidate extends BaseValidate
{
    public function rules(): array
    {
        return [
            "collection_uid" => "required|exists:ex_collection,uid",
            "record" => "required|json",
        ];
    }

    public function messages(): array
    {
        return [
            "collection_uid.required" => "试卷不能为空",
            "collection_uid.exists" => "试卷无效",
            "record.required" => "答题记录不能为空",
            "record.json" => "答题记录格式无效",
        ];
    }
}
