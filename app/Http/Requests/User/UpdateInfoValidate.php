<?php
declare(strict_types=1);

namespace App\Http\Requests\User;

use App\Http\Requests\BaseValidate;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/21
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class UpdateInfoValidate extends BaseValidate
{
    public function rules(): array
    {
        return [
            "nickname" => "required|max:20",
            "avatar_url" => "required",
            //"profession_uid" => "required|exists:config_profession,uid",
            "gender" => "required|in:0,1,2",
        ];
    }

    public function messages(): array
    {
        return [
            "nickname.required" => "个人昵称不为空",
            "nickname.max" => "昵称最大20个字符",
            "avatar_url.required" => "个人头像必传",
            "profession_uid.required" => "个人专业不能为空",
            "gender.required" => "性别不能为空",
            "gender.in" => "性别值错误",
        ];
    }
}
