<?php
declare(strict_types=1);
/**
 * @project: 兔兔兔兔答题微信小程序
 * @author: kert
 * @date: 2023/5/23
 * @link: http://www.tutudati.com/
 * @site: 微信搜索-兔兔答题
 */

namespace App\Admin\Controllers\Article;

use App\Admin\Controllers\BaseController;
use App\Library\SnowFlakeId;
use App\Model\Admin\Article\ArticleCate;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;

class CateController extends BaseController
{
    protected $title = "文章类目";

    public function grid(): Grid
    {
        $grid = new Grid(new ArticleCate());
        $grid->filter(function ($filter) {
            $filter->disableIdFilter();
            $filter->column(1 / 2, function ($filter) {
                $filter->like('title', "类目名称");
                $filter->equal('is_show', "上架状态")->select([
                    1 => '上架',
                    2 => '下架',
                ]);
            });
            $filter->column(1 / 2, function ($filter) {
                $filter->between('created_at', "创建时间")->datetime();
            });
        });
        $grid->model()->orderByDesc("id");

        $grid->column("uid", "数据编号")->copyable();
        $grid->column("title", "类目名称")->editable();
        $grid->column("sort", "显示权重")->editable();
        $grid->column('is_show', "上架状态")->switch($this->switch);
        $grid->column("seo_title", "SEO标题")->editable();
        $grid->column("created_at", "创建时间");
        $grid->disableExport();
        $grid->actions(function ($actions) {
            $actions->disableView();
        });
        return $grid;
    }

    public function form(): Form
    {
        $form = new Form(new ArticleCate());
        $form->hidden("uid", "类目编号")->default(SnowFlakeId::getId());
        $form->text("title", "类目名称")->rules('required|max:20');
        $form->number("sort", "显示顺序")->default(0)->help("值越大，显示权重越高，最大值99999999");
        $form->switch("is_show", "上架状态")->options($this->switch)->default(1);
        $form->text("seo_title", "SEO标题")->required()->rules('required|max:100')->help("字符长度最大不能超过100");
        $form->textarea("seo_keywords", "SEO关键词")->required()->rules('required|max:255')->help("字符长度最大不能超过255");
        $form->textarea("seo_description", "SEO描述")->required()->rules('required|max:255')->help("字符长度最大不能超过255");

        return $form;
    }
}
