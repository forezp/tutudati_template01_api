<?php
declare(strict_types=1);
/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/9
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */

namespace App\Admin\Controllers\Config;

use App\Admin\Controllers\BaseController;
use App\Library\SnowFlakeId;
use App\Model\Admin\Config\AdminMenu;
use Encore\Admin\Form;
use Encore\Admin\Grid;

class MenuController extends BaseController
{
    protected $title = "菜单管理";

    public function grid(): Grid
    {
        $grid = new Grid(new AdminMenu());

        $grid->paginate(15);
        $grid->model()->orderByDesc("id");
        $grid->disableFilter();

        $grid->column("uid", "数据编号")->copyable();
        $grid->column("title", "菜单名称");
        $grid->column("cover", "菜单图标")->lightbox(['width' => 50, 'height' => 50]);
        $grid->column("sort", "显示权重")->integer();
        $grid->column("navigate", "跳转地址");
        $grid->column('is_show', "上架状态")->switch($this->switch);
        $grid->column("created_at", "创建时间");
        $grid->disableExport();
        $grid->actions(function ($actions) {
            $actions->disableView();
        });

        return $grid;
    }

    public function form(): Form
    {
        $form = new Form(new AdminMenu());

        $form->hidden("uid", "图片编号")->default(SnowFlakeId::getId());
        $form->text("title", "菜单名称")->rules('required|max:20')->help("推荐长度不超过4个中文汉字");
        $form->text("navigate", "跳转地址");
        $form->hidden("url", "图片地址")->default(env("QINIU_URL"));
        $form->number("sort", "显示权重")->default(0)->min(0)->max(999)->help("值越大，权重越高。最大值999。");
        $form->switch("is_show", "上架状态")->states($this->switch)->default(1);
        $form->image("path", "菜单图标")->uniqueName()->required();

        return $form;
    }
}
