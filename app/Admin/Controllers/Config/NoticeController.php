<?php
declare(strict_types=1);

namespace App\Admin\Controllers\Config;

use App\Admin\Controllers\BaseController;
use App\Library\SnowFlakeId;
use App\Model\Admin\Config\AdminNotice;
use Encore\Admin\Form;
use Encore\Admin\Grid;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/22
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class NoticeController extends BaseController
{
    protected $title = "公告设置";

    public function grid(): Grid
    {
        $grid = new Grid(new AdminNotice());
        $grid->model()->orderByDesc("id");
        $grid->disableFilter();

        $grid->column("uid", "数据编号")->copyable();
        $grid->column("title", "公告标题");
        $grid->column("sort", "显示权重")->integer();
        $grid->column("navigate", "跳转地址");
        $grid->column("start", "开始时间");
        $grid->column("end", "结束时间");
        $grid->column('is_show', "上架状态")->switch($this->switch);
        $grid->column("created_at", "创建时间");
        $grid->disableExport();
        $grid->actions(function ($actions) {
            $actions->disableView();
        });

        return $grid;
    }

    public function form(): Form
    {
        $form = new Form(new AdminNotice());
        $form->hidden("uid", "公告编号")->default(SnowFlakeId::getId());
        $form->text("title", "公告标题")->rules('sometimes|max:32')->help("最大不要超过32个字符");
        $form->text("navigate", "跳转地址");
        $form->datetime("start", "开始时间")->required();
        $form->datetime("end", "结束时间")->required();
        $form->number("sort", "显示权重")->default(0)->rules("required|integer")->help("权重越高，显示越靠前");
        $form->switch("is_show", "上架状态")->states($this->switch)->default(1);

        return $form;
    }
}
