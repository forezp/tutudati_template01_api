<?php
declare(strict_types=1);
/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/9
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */

namespace App\Admin\Controllers\Config;

use App\Admin\Controllers\BaseController;
use App\Library\SnowFlakeId;
use App\Model\Admin\Config\AdminApp;
use Encore\Admin\Form;
use Encore\Admin\Grid;

class AppController extends BaseController
{
    protected $title = "应用配置";


    public function grid(): Grid
    {
        $grid = new Grid(new AdminApp());
        $grid->model()->paginate(15);
        $grid->disableFilter();

        $grid->column("uid", "数据编号")->copyable();
        $grid->column("type", "应用平台")->display(function ($type) {
            switch ($type) {
                case 1:
                    return "微信小程序";
                case 2:
                    return "微信公众号";
                case 3:
                    return "H5";
                case 4:
                    return "iOS";
                case 5:
                    return "Android";
                default:
                    return "未知";
            }
        });
        $grid->column("title", "应用名称");
        $grid->column("cover", "应用图标")->lightbox(['width' => 50, 'height' => 50]);
        $grid->column("navigate", "跳转地址");
        $grid->column("created_at", "创建时间");
        $grid->disableExport();
        $grid->actions(function ($actions) {
            $actions->disableView();
        });
        $grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
                $batch->disableDelete();
            });
        });

        return $grid;
    }

    public function form(): Form
    {
        $form = new Form(new AdminApp());
        $form->hidden("uid", "图片编号")->default(SnowFlakeId::getId());
        $form->radio("type", "应用平台")->options([1 => "微信小程序", 2 => "微信公众号", 3 => "H5", 4 => "iOS", 5 => "Android"])->required();
        $form->text("title", "配置名称")->rules('required|max:20')->help("推荐长度不超过4个中文汉字");
        $form->text("navigate", "跳转地址");
        $form->hidden("url", "图片地址")->default(env("QINIU_URL"));
        $form->image("path", "应用logo")->uniqueName()->required();
        $form->table('value', "应用配置", function ($table) {
            $table->text('key', "配置名称")->required()->help("配置一旦在客户端使用，就不要修改对应的配置名称，如果修改配置名称，相应接口需要根据实际情况进行变更");
            $table->text('value', "配置值")->required();
            $table->text('desc', "配置描述");
        });

        return $form;
    }
}
