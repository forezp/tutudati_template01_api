<?php

declare(strict_types=1);

namespace App\Admin\Controllers\Message;

use App\Admin\Controllers\BaseController;
use App\Model\Admin\Message\AdminTemplate;
use Encore\Admin\Grid;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/25
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class TemplateController extends BaseController
{
    protected $title = "订阅记录";

    public function grid(): Grid
    {
        $grid = new Grid(new AdminTemplate());
        $grid->filter(function ($filter) {
            $filter->disableIdFilter();
            $filter->column(1 / 2, function ($filter) {
                $filter->equal("is_send", "发送状态")->select([1 => "待发送", 2 => "已发送"]);
                $filter->equal("send_result", "发送结果")->select([1 => "发送成功", 2 => "发送失败"]);
            });
            $filter->column(1 / 2, function ($filter) {
                $filter->like("openid", "微信openid");
                $filter->like("template_id", "微信模板");
                $filter->between('created_at', "订阅时间")->datetime();
            });
        });
        $grid->model()->orderByDesc("id")->paginate(15);
        $grid->column("uid", "数据编号")->copyable();
        $grid->column("user.avatar_url", "用户头像")->lightbox(['width' => 50, 'height' => 50]);
        $grid->column("user.nickname", "用户昵称");
        $grid->column("user.name", "用户姓名");
        $grid->column("openid", "微信openid")->copyable();
        $grid->column("template_id", "微信模板")->copyable();
        $grid->column("is_send", "发送状态")->display(function ($is_send) {
            if ($is_send == 1) {
                return "<span style='color:blue'>待发送</span>";
            }
            return "<span style='color:gray'>已发送</span>";
        });
        $grid->column("send_result", "发送结果")->display(function ($send_result) {
            if ($send_result == 1) {
                return "<span style='color:blue'>发送成功</span>";
            }
            return "<span style='color:gray'>发送失败</span>";
        });
        $grid->column("send_msg", "发送结果信息");
        $grid->column("created_at", "订阅时间");

        $grid->actions(function ($actions) {
            $actions->disableView();
            $actions->disableDelete();
            $actions->disableEdit();
        });
        $grid->disableCreateButton();
        $grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
                $batch->disableDelete();
            });
        });

        return $grid;
    }
}
