<?php
declare(strict_types=1);

namespace App\Admin\Controllers\Exam;

use App\Admin\Controllers\Config\BannerController;
use App\Library\SnowFlakeId;
use App\Model\Admin\Exam\AdminCategory;
use App\Model\Admin\Exam\AdminCollection;
use App\Model\Admin\Exam\AdminReading;
use Encore\Admin\Form;
use Encore\Admin\Grid;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/19
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class ReadingController extends BannerController
{
    protected $title = "问答试题";

    public function grid(): Grid
    {
        $grid = new Grid(new AdminReading());
        $grid->filter(function ($filter) {
            $filter->disableIdFilter();
            $filter->column(1 / 2, function ($filter) {
                $filter->like('title', "试题问题");
                $filter->equal('is_show', "上架状态")->select([
                    1 => '上架',
                    2 => '下架',
                ]);
            });
            $filter->column(1 / 2, function ($filter) {
                $filter->between('created_at', "创建时间")->datetime();
                $filter->equal('is_free', "是否免费")->select([
                    1 => '是',
                    2 => '否',
                ]);
            });
        });
        $grid->model()->orderByDesc("id")->paginate(20);
        $grid->column("uid", "数据编号")->copyable();
        $grid->column("category.title", "试题分类");
        //$grid->column("collection.title", "试卷名称");
        $grid->column("title", "试题问题");
        $grid->column("sort", "显示权重")->integer();
        $grid->column("score", "试题积分");
        $grid->column('is_show', "上架状态")->switch($this->switch);
        $grid->column('is_free', "是否免费")->switch($this->isFree);
        $grid->column("submit", "答题人数")->sortable();
        $grid->column("collect", "收藏人数")->sortable();
        $grid->column("created_at", "创建时间");
        $grid->disableExport();
        $grid->actions(function ($actions) {
            $actions->disableView();
        });
        return $grid;
    }

    public function form(): Form
    {
        $form = new Form(new AdminReading());
        $form->hidden("uid", "试卷编号")->default(SnowFlakeId::getId());
        $form->radio("category_uid", "试题分类")->options(AdminCategory::getList())->required();
        $form->multipleSelect("collection", "试题试卷")->options(AdminCollection::getList());
        $form->text("title", "试题问题")->rules('required|max:77')->help("最大字符长度77。");
        $form->number("sort", "显示权重")->default(0)->help("值越大，显示权重越高");
        $form->switch("is_show", "上架状态")->states($this->switch)->default(1);
        $form->switch("is_free", "是否免费")->states($this->isFree)->default(2);
        $form->decimal("score", "试题积分")->help("试题积分不能小于0")->required()->value(9);
        $form->UEditor('content', "试题答案")->options(['initialFrameHeight' => 400])->required();

        return $form;
    }
}
