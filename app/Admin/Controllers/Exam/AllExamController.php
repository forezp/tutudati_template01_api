<?php
declare(strict_types=1);

namespace App\Admin\Controllers\Exam;

use App\Admin\Actions\Exam\BindCollectionSubmitAction;
use App\Admin\Controllers\BaseController;
use App\Model\Admin\Exam\AdminOption;
use Encore\Admin\Grid;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

/**
 * 试卷库
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/19
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class AllExamController extends BaseController
{
    protected $title = "试卷库";

    public function grid(): Grid
    {
        $grid = new Grid(new AdminOption());
        $judeExamBuilder = DB::table("ex_jude")->where("is_show", "=", 1)
            ->whereNull("deleted_at")
            ->select(["uid", "title", "created_at", DB::raw("2 as exam_type")]);
        $reddingExamBuilder = DB::table("ex_reading")->where("is_show", "=", 1)
            ->whereNull("deleted_at")
            ->select(["uid", "title", "created_at", DB::raw("3 as exam_type")]);
        $grid->disableFilter();
        $grid->model()
            ->unionAll($judeExamBuilder)
            ->unionAll($reddingExamBuilder)
            ->where("is_show", "=", 1)
            ->select(["uid", "title", "created_at", DB::raw("1 as exam_type")])
            ->orderByDesc("created_at");

        $collectionUid = request()->all("collection_uid");
        Cache::put("admin:s:c:uid", $collectionUid);

        $grid->column("uid", "试题编号");
        $grid->column("exam_type", "试题类型")->display(function ($type) {
            switch ($type) {
                case 1:
                    return  "<span style='color:#82B2FF;'>单选试题</span>";
                case 2:
                    return  "<span style='color:#FF71D2;'>判断试题</span>";
                case 3:
                    return  "<span style='color:#FF7043;'>阅读试题</span>";
                default:
                    return  "";
            }
        });
        $grid->column("title", "试题题干");
        $grid->column("created_at", "创建时间");

        $grid->actions(function ($actions) {
            $actions->disableView();
            $actions->disableDelete();
            $actions->disableEdit();
            $actions->add(new BindCollectionSubmitAction());
        });
        $grid->disableExport();
        $grid->disableCreateButton();
        $grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
                $batch->disableDelete();
            });
        });

        return $grid;
    }
}
