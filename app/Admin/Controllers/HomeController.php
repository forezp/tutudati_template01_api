<?php
declare(strict_types=1);

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Content;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/5/16
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class HomeController extends Controller
{
    public function index(Content $content): Content
    {
        return $content;
    }
}
