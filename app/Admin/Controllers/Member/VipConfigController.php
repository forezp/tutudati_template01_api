<?php
declare(strict_types=1);

namespace App\Admin\Controllers\Member;

use App\Admin\Controllers\BaseController;
use App\Library\SnowFlakeId;
use App\Model\Admin\Member\AdminConfig;
use Encore\Admin\Form;
use Encore\Admin\Grid;

/**
 * 会员等级配置
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/17
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class VipConfigController extends BaseController
{
    protected $title = "会员配置";

    public function grid(): Grid
    {
        $grid = new Grid(new AdminConfig());

        $grid->filter(function ($filter) {
            $filter->disableIdFilter();
            $filter->column(1 / 2, function ($filter) {
                $filter->like('title', "试卷名称");
                $filter->equal('is_show', "上架状态")->select([
                    1 => '上架',
                    2 => '下架',
                ]);
            });
            $filter->column(1 / 2, function ($filter) {
                $filter->between('created_at', "创建时间")->datetime();
                $filter->equal('recommend', "是否推荐")->select([
                    1 => '是',
                    2 => '否',
                ]);
            });
        });
        $grid->model()->orderByDesc("id")->paginate(10);

        $grid->column("uid", "数据编号")->copyable();
        $grid->column("title", "会员名称")->editable();
        $grid->column("money_unit", "会员价格");
        $grid->column("remark", "会员描述")->editable();
        $grid->column("sort", "显示权重")->integer();
        $grid->column('is_show', "上架状态")->switch($this->switch);
        $grid->column("created_at", "创建时间");

        return $grid;
    }

    public function form(): Form
    {
        $form = new Form(new AdminConfig());

        $form->hidden("uid", "数据uid")->default(SnowFlakeId::getId());
        $form->text("title", "会员名称")->required();
        $form->decimal("money", "会员价格")->required();
        $form->select("unit", "价格单位")->options(["小时" => "小时", "天" => "天", "周" => "周", "月" => "月", "季度" => "季度", "年度" => "年度", "终身" => "终身"]);
        $form->number("sort", "显示权重")->default(0)->help("值越大，显示权重越高");
        $form->switch("is_show", "上架状态")->states($this->switch)->default(1);
        $form->textarea("remark", "会员描述")->required();
        $form->UEditor('equity', "会员权益")->required()->help("适用于题干中带有图片，视频等场景")->options(['initialFrameHeight' => 200]);

        return $form;
    }
}
