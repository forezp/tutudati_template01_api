<?php
/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/5/16
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
app('view')->prependNamespace('admin', resource_path('views/admin'));
Encore\Admin\Form::forget(['map', 'editor']);
