<?php
declare(strict_types=1);

namespace App\Model\User\Exam;

use App\Model\Common\Exam\Submit;
use App\Model\User\User\UserModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * 答题汇总记录
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/8/04
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class UserSubmit extends Submit
{
    public function collection(): BelongsTo
    {
        return $this->belongsTo(UserCollection::class, "collection_uid", "uid");
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(UserModel::class, "user_uid", "uid");
    }
}
