<?php
declare(strict_types=1);

namespace App\Model\User\Member;

use App\Model\Common\Member\Config;

/**
 * 会员等级配置
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/17
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class UserConfig extends Config
{
    
}