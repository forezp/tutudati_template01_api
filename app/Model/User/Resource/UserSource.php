<?php
declare(strict_types=1);

namespace App\Model\User\Resource;

use App\Model\Common\Resource\Resource;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/5/25
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class UserSource extends Resource
{

}
