<?php
declare(strict_types=1);

namespace App\Model\Common\Document;

use App\Model\Admin\Document\AdminDocumentGroup;
use App\Model\Common\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/23
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class Document extends BaseModel
{
    protected $table = "document";

    protected $fillable = [
        "title",
        "content",
        "is_show",
        "sort",
        "group_uid",
    ];

    protected $casts = [
        "uid" => "string",
        "group_uid" => "string",
    ];

    public function group(): BelongsTo
    {
        return $this->belongsTo(AdminDocumentGroup::class, "group_uid", "uid");
    }
}
