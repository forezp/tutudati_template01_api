<?php
declare(strict_types=1);

namespace App\Model\Common\Exam;

use App\Model\Common\BaseModel;
use App\Model\Common\User\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/22
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class ReadingSubmitHistory extends BaseModel
{
    protected $table = "ex_reading_submit_history";

    protected $fillable = [
        "uid",
        "reading_uid",
        "user_uid",
        "is_show",
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, "user_uid", "uid");
    }
}
