<?php
declare(strict_types=1);

namespace App\Model\Common\Exam;

use App\Model\Common\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/17
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class Collection extends BaseModel
{
    protected $table = "ex_collection";

    protected $fillable = [
        "uid",
        "title",
        "is_show",
        "sort",
        "url",
        "path",
        "collect",
        "submit",
        "recommend",
        "content",
        "category_uid",
        "level",
        "author",
        "remark",
        "mini_qr_code",
        "picture",
        "year"
    ];

    protected $casts = [
        "uid" => "string",
        "category_uid" => "string",
    ];

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, "category_uid", "uid");
    }

    public function getPictureAttribute($pictures): array
    {
        if (!empty($pictures)) {
            return json_decode($pictures, true);
        } else {
            return [];
        }
    }

    public function getYearAttribute($key): string
    {
        return !empty($key) ? (string)$key : "";
    }

    public function getRemarkAttribute($key): string
    {
        return !empty($key) ? $key : "";
    }

    public function getMiniQrCodeAttribute($key): string
    {
        return !empty($key) ? $key : "";
    }

    public function getPathAttribute($key): string
    {
        return $key;// . "?imageView2/2/format/webp/q/95!";
    }
}
