<?php
declare(strict_types=1);

namespace App\Model\Common\Exam;

use App\Model\Common\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/19
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class Reading extends BaseModel
{
    protected $table = "ex_reading";

    protected $fillable = [
        "title",
        "is_show",
        "sort",
        "collection_uid",
        "is_free",
        "content",
        "category_uid",
        "submit",
        "collect",
        "is_free",
        "score",
    ];

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, "category_uid", "uid");
    }

    public function collection(): BelongsToMany
    {
        return $this->belongsToMany(Collection::class, "ex_collection_reading_rel",
            "exam_uid", "collection_uid", "uid", "uid");
    }
}
