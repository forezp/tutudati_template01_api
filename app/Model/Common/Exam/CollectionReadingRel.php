<?php
declare(strict_types=1);

namespace App\Model\Common\Exam;

use Illuminate\Database\Eloquent\Model;

/**
 * 判断试题关联
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/5/24
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class CollectionReadingRel extends Model
{
    protected $table = "ex_collection_reading_rel";

    protected $fillable = [
        "collection_uid",
        "exam_uid",
    ];
}
