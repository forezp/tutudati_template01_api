<?php
declare(strict_types=1);
/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/9
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */

namespace App\Model\Common\Config;

use App\Model\Common\BaseModel;

class Document extends BaseModel
{
    protected $table = "document";

    protected $fillable = [
        "title",
        "content",
        "is_show",
    ];
}
