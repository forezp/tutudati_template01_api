<?php

declare(strict_types=1);

namespace App\Model\Common\Message;

use App\Model\Common\BaseModel;
use App\Model\Common\User\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/5/25
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class Template extends BaseModel
{
    protected $table = "template_subscribe";

    protected $fillable = [
        "user_uid",
        "openid",
        "template_id",
        "uid",
        "is_send",
        "send_result",
        "send_msg",
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, "user_uid", "uid");
    }

    public function getSendMsgAttribute($key): string
    {
        return !empty($key) ? $key : "";
    }
}
