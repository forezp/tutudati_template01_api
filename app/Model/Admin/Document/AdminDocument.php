<?php
declare(strict_types=1);

namespace App\Model\Admin\Document;


use App\Model\Common\Document\Document;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/23
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class AdminDocument extends Document
{

}
